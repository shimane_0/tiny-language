package yyvm;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Path;
import javafx.stage.Stage;


public class Example extends Application{
	public static void main(String[] args){
		//System.out.println(args[0]);
		launch("test");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("YYVM");

		Group root = new Group();
		Scene scene = new Scene(root, 1000, 600);
		scene.setFill(null);
		YYView aView = new YYView();
		//ArrayList<Path> aCollection = new ArrayList<Path>();
		//aCollection.add(aView.drawPath());
		//aCollection.add(aView.drawPath2());
		
		root.getChildren().add(aView.drawArc());
		//root.getChildren().add(aView.drawPath());
		root.getChildren().add(aView.drawRectangle());
		//root.getChildren().addAll(aCollection);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
