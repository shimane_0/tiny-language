package yyvm;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;

/**
 * ビュー。
 */
public class YYView implements Initializable{
    public Path drawPath() {
        Path path = new Path(
        		new MoveTo(100.0,200.0),
        		new LineTo(140.0,300.0),
        		new LineTo(160.0,200.0),
        		new LineTo(200.0,300.0)
        );
 
        return path;
    }
    /*
    public Path drawPath2() {
        Path path = new Path(
        		new MoveTo(400.0,200.0),
        		new LineTo(440.0,300.0),
        		new LineTo(460.0,200.0),
        		new LineTo(500.0,300.0)
        );
 
        return path;
    }
    */
    public Arc drawArc(){
		Arc anArc = new Arc();
		anArc.setCenterX(50.0f);
		anArc.setCenterY(50.0f);
		anArc.setRadiusX(25.0f);
		anArc.setRadiusY(25.0f);
		anArc.setStartAngle(45.0f);
		anArc.setLength(270.0f);
		anArc.setStroke(Color.BLACK);
		anArc.setStrokeWidth(5.0);
		anArc.setFill(Color.WHITE);
		anArc.setType(ArcType.OPEN);
		return anArc;
    }
    public Rectangle drawRectangle(){
		Rectangle aRectangle = new Rectangle(100, 100, 100, 50);
		aRectangle.setStrokeWidth(3.0);
		aRectangle.setStroke(Color.RED);
		aRectangle.setFill(Color.WHITE);
		return aRectangle;
    }
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.drawRectangle();
	}
}
